## 1.3.2
- Compiled the SDK with bitcode enabled

## 1.3.1
- Removed the CardIO Pod from podspec dependencies of the Fidel SDK.

## 1.3.0
Compiled with Swift 4.1.2.
- Added USA country option.
- Made the SDK available for Objective-C projects.
- Made the Pod available for the public, through the Cocoapods trunk.
- Made it easier to setup and use the Pod.
- Made the CardIO Pod an external dependency of the Fidel SDK.

## 1.2.3
- Compiled the SDK with bitcode enabled

## 1.2.2
- Removed the CardIO Pod from podspec dependencies of the Fidel SDK.

## 1.2.1
Compiled with Swift 4.0.2.
- Added USA country option.
- Made the SDK available for Objective-C projects.
- Made the Pod available for the public, through the Cocoapods trunk.
- Made it easier to setup and use the Pod.
- Made the CardIO Pod an external dependency of the Fidel SDK.

## 1.2.0
Migrated to Swift 4.0.

## 1.1.3
- Compiled the SDK with bitcode enabled

## 1.1.2
- Removed the CardIO Pod from podspec dependencies of the Fidel SDK.

## 1.1.1
Compiled with Swift 3.0.
- Added USA country option.
- Made the SDK available for Objective-C projects.
- Made the Pod available for the public, through the Cocoapods trunk.
- Made it easier to setup and use the Pod.
- Made the CardIO Pod an external dependency of the Fidel SDK.

## 1.1.0
Migrated to Swift 3.2.

## 1.0.4

Features:

- Added option to use SDK in test environment using test card numbers

Improvements:

- Replaced UI assets
- Refactored comments
- Updated README.md
- Included CHANGELOG.md

## 1.0.3

Bugfixes:

- Fixed new callback name tests

## 1.0.2

Improvements:

- Replaced cardId and error callbacks with LinkResult and LinkError

## 1.0.1

Improvements:

- Updated Example app

Bugfixes:

- Fixed card.io camera nil image

## 1.0.0

Features:

- Added camera card scanning
- Added API link card endpoints
- Banner image customisation
- Added country code picker
- API tests
